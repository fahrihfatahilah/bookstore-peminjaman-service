FROM openjdk:17

WORKDIR /app

COPY target/peminjamanservice-0.0.1-SNAPSHOT.jar /app/peminjamanservice-0.0.1.jar

EXPOSE 8082

CMD ["java", "-jar", "peminjamanservice-0.0.1.jar"]