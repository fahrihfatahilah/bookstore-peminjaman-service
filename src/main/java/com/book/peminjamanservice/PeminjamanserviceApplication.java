package com.book.peminjamanservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeminjamanserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeminjamanserviceApplication.class, args);
	}

}
