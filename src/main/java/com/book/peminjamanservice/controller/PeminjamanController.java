package com.book.peminjamanservice.controller;

// BorrowingController.java
import com.book.peminjamanservice.model.BaseResponse;
import com.book.peminjamanservice.model.Peminjaman;
import com.book.peminjamanservice.service.PeminjamanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/peminjaman")
public class PeminjamanController {

    private static final Logger log = LoggerFactory.getLogger(PeminjamanController.class);
    private final PeminjamanService peminjamanService;

    @Autowired
    public PeminjamanController(PeminjamanService peminjamanService) {
        this.peminjamanService = peminjamanService;
    }

    @GetMapping
    public ResponseEntity<BaseResponse<List<Peminjaman>>> getAllPeminjamans() {
        List<Peminjaman> borrowings = peminjamanService.getAllPeminjamans();

        System.out.println("pinjaman" + borrowings);
        BaseResponse<List<Peminjaman>> response = new BaseResponse<>("ok", HttpStatus.OK.value(), borrowings);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/pinjamanById/{id}")
    public ResponseEntity<BaseResponse<List<Peminjaman>>> getPeminjamanById(@PathVariable Long id) {
        List<Peminjaman> borrowing = peminjamanService.getPeminjamanById(id);
        BaseResponse<List<Peminjaman>> response = new BaseResponse<>("ok", HttpStatus.OK.value(), borrowing);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<String>> createPeminjaman(@RequestBody Peminjaman borrowing) {
        borrowing.setBorrowing_date(new Date());
        peminjamanService.createPeminjaman(borrowing);
        BaseResponse<String> response = new BaseResponse<>("ok", HttpStatus.CREATED.value(), "Peminjaman created successfully");
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PostMapping("/returnPinjaman/{id}")
    public ResponseEntity<BaseResponse<String>> returnPinjaman(@RequestBody Peminjaman borrowing, @PathVariable long id) {
        borrowing.setReturn_date(new Date());
        borrowing.setId(id);
        peminjamanService.returnPinjaman(borrowing);
        BaseResponse<String> response = new BaseResponse<>("ok", HttpStatus.CREATED.value(), "Return successfully");
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<String>> updatePeminjaman(@PathVariable Long id, @RequestBody Peminjaman borrowing) {
        borrowing.setId(id);
        peminjamanService.updatePeminjaman(borrowing);
        BaseResponse<String> response = new BaseResponse<>("ok", HttpStatus.OK.value(), "Peminjaman updated successfully");
        return ResponseEntity.ok(response);
    }

    @GetMapping("deletePinjaman/{id}")
    public ResponseEntity<BaseResponse<String>> deletePeminjaman(@PathVariable Long id) {
        BaseResponse<String> response;
        try {
            peminjamanService.deletePeminjaman(id);
            response = new BaseResponse<>("ok", HttpStatus.OK.value(), "Peminjaman deleted successfully");
        }catch (Exception e){
            response = new BaseResponse<>("error", HttpStatus.OK.value(), "Id doesnt exist");
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/peminjamanByMemberId/{id}")
    public ResponseEntity<BaseResponse<List<Peminjaman>>> getListPinjamanByMemberId(@PathVariable Long id) {
        List<Peminjaman> borrowings = peminjamanService.getPeminjamanByMemberId(id);
        BaseResponse<List<Peminjaman>> response = new BaseResponse<>("ok", HttpStatus.OK.value(), borrowings);
        return ResponseEntity.ok(response);
    }
}
