package com.book.peminjamanservice.mapper;

import com.book.peminjamanservice.model.Peminjaman;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PeminjamanMapper {
    @Select("SELECT * FROM peminjaman")
    List<Peminjaman> getAllPeminjamans();

    @Select("SELECT * FROM peminjaman WHERE id = #{id}")
    List<Peminjaman> getPeminjamanById(Long id);

    @Insert("INSERT INTO peminjaman(member_id, book_id, borrowing_date) " +
            "VALUES(#{member_id}, #{book_id}, #{borrowing_date})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertPeminjaman(Peminjaman borrowing);

    @Update("UPDATE peminjaman SET member_id = #{memberId}, book_id = #{book_id}, " +
            "borrowing_date = #{borrowing_date}, return_date = #{return_date} WHERE id = #{id}")
    void updatePeminjaman(Peminjaman borrowing);

    @Delete("DELETE FROM peminjaman WHERE id = #{id}")
    void deletePeminjaman(Long id);

    @Select("SELECT * FROM peminjaman WHERE member_id = #{member_id}")
    List<Peminjaman> getPeminjamanByMemberId(Long memberId);

    @Update("UPDATE peminjaman SET return_date = #{return_date} WHERE id = #{id} AND book_id = #{book_id}")
    void returnPinjaman(Peminjaman borrowing);
}
