package com.book.peminjamanservice.service;

import com.book.peminjamanservice.mapper.PeminjamanMapper;
import com.book.peminjamanservice.model.Peminjaman;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeminjamanService {

    private static final Logger log = LoggerFactory.getLogger(PeminjamanService.class);

    private final PeminjamanMapper peminjamanMapper;

    @Autowired
    public PeminjamanService(PeminjamanMapper peminjamanMapper) {
        this.peminjamanMapper = peminjamanMapper;
    }

    public List<Peminjaman> getAllPeminjamans() {
        return peminjamanMapper.getAllPeminjamans();
    }

    public List<Peminjaman> getPeminjamanById(Long id) {
        return peminjamanMapper.getPeminjamanById(id);
    }

    public void createPeminjaman(Peminjaman borrowing) {
        peminjamanMapper.insertPeminjaman(borrowing);
    }

    public void returnPinjaman(Peminjaman borrowing) {
        peminjamanMapper.returnPinjaman(borrowing);
    }

    public void updatePeminjaman(Peminjaman peminjaman) {
        peminjamanMapper.updatePeminjaman(peminjaman);
    }

    public void deletePeminjaman(Long id) throws Exception {
        List<Peminjaman> peminjamanList = peminjamanMapper.getPeminjamanById(id);
        if (peminjamanList.isEmpty()) {
            throw new Exception("Peminjaman with ID " + id + " does not exist");
        }

        Peminjaman peminjaman = peminjamanList.get(0);

        peminjamanMapper.deletePeminjaman(peminjaman.getId());
    }

    public List<Peminjaman> getPeminjamanByMemberId(Long memberId) {
        return peminjamanMapper.getPeminjamanById(memberId);
    }
}
