package com.book.peminjamanservice.model;

import lombok.Data;

import java.util.Date;

@Data
public class Peminjaman {


    private Long id;
    private Long member_id;
    private Long book_id;
    private Date borrowing_date;
    private Date return_date;

}
