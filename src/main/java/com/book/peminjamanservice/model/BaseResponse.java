package com.book.peminjamanservice.model;

public class BaseResponse<T> {
    private String status;
    private int responseCode;
    private T data;
    private String message;


    public BaseResponse(String status, int responseCode, T data) {
        this.status = status;
        this.responseCode = responseCode;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
